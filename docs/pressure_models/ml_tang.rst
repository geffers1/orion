Pretrained ML Pressure Model
============================

The pretrained ML pressure model is currently designed to work with a 2D ML model developed by Hewei Tang at LLNL.
This model requires estimates of permeability, :math:`K`, across the reservoir and a set of scaling parameters.
Note: The model path can either point to a HDF5 format model or a ZIP file containing the model directory (required for some models with custom layers).

.. image:: ../figures/configure_pressure_pretrained_ml.png
