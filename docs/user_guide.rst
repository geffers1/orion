##########
User Guide
##########

Installation from Source
===========================

To install Orion from it's source you must first clone the gitlab repository.
You can then install it within an existing Python environment using `pip`:

.. code-block:: bash

   git clone git@gitlab.com:NRAP/orion.git
   pip install .

Note: On shared machines, we recommend that you install Orion within a virtual Python environment to avoid dependency issues with other tools.


Optional Features (PyCSEP)
--------------------------

Some optional features (fetching ComCat catalogs from the Internet, etc) require :mod:`pycsep`, which has some non-Python pre-requisites that may not be present on your system (proj, geos, shapely).
To setup your environment, we recommend first following the steps in the `PyCSEP Documentation <https://docs.cseptesting.org/getting_started/installing.html>`_.
After this, Orion can be installed via `pip`` with the optional features:

.. code-block:: bash

   git clone git@gitlab.com:NRAP/orion.git
   pip install .[pycsep]


Pre-compiled Version (Experimental)
=====================================

For certain systems, pre-compiled versions of Orion are available on `EDX <https://edx.netl.doe.gov/workspace/resources/orion?folder_id=adb0a225-94b0-4fed-a19c-88d56d6a8c45>`_.
These files are portable, so they simply need to be downloaded to the local machine and executed.
If you encounter any issues running these versions of orion, try running the *debug* version of the executable, which launches a console that will display potential error messages.


Running Orion
=============

To open the Orion GUI, run the following from the command-line:

.. code-block:: bash

   orion_forecast


Alternately, Orion can be run without the GUI by specifying the following options:

.. code-block:: bash

   orion_forecast --no_gui --config filename.json


As it is running, Orion will maintain a copy of its configuration in the user cache (located at `~/.cache/orion/orion_config.json`).
When opening Orion, the code will attempt to resume using this file.
If you encounter any error opening Orion, we recommend that you delete the cached file and try again.


Orion GUI
=========

When running the Orion GUI, a new window will open on the local machine.
At the top of the window, there are a set of dropdown menus that can be used to load data, save figures, open the configuration window, and load a set of pre-constructed examples.
We recommend that users load the 2014 Cushing Oklahoma example by selecting *Model/Built In/Oklahoma_Cushing_2014*, and then inspect the configuration window by selecting *Model/Configure*.

Once Orion is configured, you can trigger pressure and/or seismic forecast calculations using one of the three buttons at the bottom of the interface.
As these calculations complete, they will create and populate the figures in the main body of the interface, which is organized into a set of tabs: *Spatial Forecast*, *Seismic Catalog*, *Forecast Models*, *Pressure*, *Fluid Injection*, *Geologic Model*, and *Log*.
For figures that display snapshots of data over time, the active time can be set via the time-slider at the bottom of the interface or via the configuration window.


Spatial Forecast Tab
--------------------

The Spatial Forecast page displays spatial forecast information on the user-defined grid, well locations, and the location of seismic events (up to the active time).
To the right of figure, there is a set of checkboxes that can be used to turn on/off the various layers of the plot.
The *Seismic Forecast* layer shows the estimated seismic event frequency (number of events per year) at points in the grid, and the *Probability* layer shows the estimated likelihood that an event greater than a target magnitude will occur during the next period of time.
The minimum magnitude and time frame can be set in the configuration window under *Forecast Models* (*Time range*, *Dial magnitude*).

.. image:: ./figures/spatial_forecast_tab.png


Seismic Catalog
---------------

The *Seismic Catalog* page displays key information about the active seismic catalog:

- Map view: This shows the location and magnitude of events. The data are projected onto a local UTM grid, with a local origin in the southwest.
- Magnitude distribution: This shows how often different sized seismic events occur. The red trend-line shows the current fit for the Gutenberg-Richter parameters, and the red triangle indicates the smallest magnitude where we have a complete catalog.
- Time series: This shows the size of events over time in days, with the origin set to the first event measured in the catalog.
- b-value variations: This shows how the Gutenberg-Richter b-value changes over time, which believed to be a key indicator of future seismic activity.

.. image:: ./figures/seismic_catalog_tab.png


Forecast Models
---------------

The *Forecast Models* page displays the results of the seismic forecast models for the entire domain.
These include:

- A bar chart showing the estimated likelihood that an event greater than a set of target magnitudes will occur during the next period of time. The magnitude distribution and time frame can be set via the *Configuration* window under *Forecast Models* (*Time range*, *Bar chart bins*)
- A dial plot showing the estimated likelihood for a target magnitude. As before, the parameters for this plot can be set via the *Configuration* window under *Forecast Models* (*Time range*, *Dial magnitude*)
- A time series plot showing the observed number of events, the estimated number of events produced by each forecast model, and an ensemble forecast. Layers can be turned on/off via the checkboxes to the right of the plot.

.. image:: ./figures/forecast_model_tab.png


Pressure
--------

The *Pressure* page displays the results of the active fluid pressure model for the entire domain, including the current pressure and pressurization rate (dpdt).
This page also displays the active well locations and the current seismic events.

.. image:: ./figures/pressure_tab.png


Fluid Injection
---------------

The *Fluid Injection* page includes four figures:

- Well location: The location of the active wells
- Flow rate: The fluid injection rate by well over time
- Fluid volume: The overall fluid volume injected into the reservoir over time
- Pressure: The estimated pressure at the monitor wells over time

.. image:: ./figures/fluid_injection_tab.png


Well Database
-------------

The *Well Database* shows the location of wells pulled from external data sources.
It also shows the location of active wells in the model, the grid extents, and current seismic activity.
The external well database can be updated under :ref:`Well Database Configuration`.

.. image:: ./figures/well_database_tab.png


Geologic Model
--------------

The *Geologic Model* page includes other key information about the reservoir, including permeability.
Note: For now, only the machine learning (ML) based models use these data.

.. image:: ./figures/geologic_model_tab.png


Log
---

The *Log* page displays messages produced by Orion.
The types of messages presented here can be set in *Model/Configure/General*.

.. image:: ./figures/log_tab.png


Code Configuration
==================

Orion can be configured either using the GUI (recommended for new users) or via a JSON configuration file.
When you exit the code or manually save, Orion will write the configuration JSON file to the user cache (*~/.cache/orion*) or to a user-requested location.
When orion_forecast is called with the `--config filename.json` argument, Orion will apply this configuration after launching.
If this argument is not given, and a cached configuration file is detected, then Orion will attempt to apply this file instead.

To open the GUI configuration menu, select *Menu/Configure* at the top of the main screen.
Like the main Orion window, the main body of the configuration interface contains tabs, which can be selected to display key information.
When the configuration window is closed, or the *Apply* button is pressed, the changes will be sent to Orion, and any active figures will be updated.
The configuration can also be saved to or loaded from a file by clicking on the *Save Config* and *Load Config* buttons.


Orion Configuration
-------------------

The default page for the configuration window, *Orion*, contains settings that control the behavior of Orion and the active plots.
These include the snapshot time for active plots, the point size to be used for plots, the level of log messages to be produced, and the location of the log file.

.. image:: ./figures/configure_orion.png


General Configuration
---------------------

The general page contains settings for the reference time and spatio-temporal grid.
Internally, the Orion forecasting engine works with relative timestamps, with `t = 0s` indicating the *present* time.
If the *Reference Time* parameter is left blank, then Orion will use the actual current time in its calculation.
Otherwise, this parameter can be set to a value in the past, to allow for testing or *retrospective* forecasting.
The *Time range* and *Plot range* parameters define the temporal grid and plot extents for the analysis, and are specified in relative time in days.

The *X Range*, *Y Range*, and *Z Range* parameters are specified relative to the *Spatial Origin* parameter.
Other spatial input parameters in Orion are typically given as absolute locations in the UTM grid or in latitude/longitude.

If you are using the UTM option, you may need to set the *zone* (e.g., `14S`) when requesting catalog data from ComCat.
This parameter will be automatically calculated if you are using a catalog on the local machine that includes latitude/longitude information.

Note: if the checkbox labeled *Permit Modification* is selected, Orion may attempt to modify the grid dimensions to match the extents of certain input files.

.. image:: ./figures/configure_general.png


Seismic Catalog Configuration
-----------------------------

The Seismic Catalog page contains the location of the target catalog, or instructions to fetch it.

The *Catalog Path* entry can be used to specify the path to a catalog on the local machine.
Clicking on the *:* button to the right of this box will open a file explorer, which you can use to navigate to the target file.
See :ref:`Seismic Catalog Files` for a description of the available formats.

If *Catalog Path* is empty and *Use ComCat* is checked, then Orion will attempt to use :mod:`pycsep`` (an optional pre-requisite) to fetch the catalog from the Internet.
The bounds of this query in time and space are the same as the Orion grid (see the previous section), and the minimum catalog is set via *Min Magnitude Request*.

.. image:: ./figures/configure_seismic_catalog.png


Forecast Model Configuration
----------------------------

The Forecast Models page contains a set of nested set of tabs, which control the general settings for forecast calculations and forecast-specific parameters.
The checkbox at the top of the main page can be used to switch between cumulative and instantaneous forecast calculations.
The *Forecast Calculation* dropdown box sets the ensemble forecasting method:

- Use Grid: This approach will calculate the child forecasts on the user-defined grid, and then use a linear model to fit the past or retrospective forecast to the actual data.
- ML Style: This approach will split the past data into training, testing, and validation segments similar to how ML methods are trained. It will then use a model to fit the training data, relying on the other segments to estimate the ensemble model accuracy (Note: this method is in development).

The *Magnitude Exceedance Plots* section includes settings for various probabilistic calculations, which look at the probability of a seismic event exceeding a target magnitude (*Dial Magnitude* or *Bar Chart Bins*) over the next time period (*Time Range*).
Some of these plots use a stoplight coloring system, which is configured via *Color Thresholds*.

The two checkboxes at the bottom of the main page can be used to modify the forecast calculation scheme.
The *Permissive* option will instruct Orion to catch any error produced by the forecasting model (otherwise the code could exit if it encounters an error).
The *Parallel calculations* option will instruct Orion to calculate the active forecast models in parallel, which can help to speed up the calculation and keep the GUI responsive.
Note: See the specific forecast model documentation for information on their configuration parameters.

.. image:: ./figures/configure_forecast.png


Pressure Model Configuration
----------------------------

The *Pressure* page contains a set of nested set of tabs, which control the general settings for fluid pressure calculations and pressure model-specific parameters.
The main page contains a dropdown box *Pressure Model* where you can select the active model to be used in the calculation.
This page also includes a *Permissive* checkbox, which will instruct Orion to catch any error produced by the active pressure model (otherwise the code could exit if it encounters an error).
Note: See the documentation for specific pressure models for information on their configuration parameters.

.. image:: ./figures/configure_pressure.png


Fluid Injection Model Configuration
-----------------------------------

The *Fluid Injection* page contains a set of nested set of tabs, which hold the configuration for wells in Orion.
At the bottom of the page, there are three buttons that will allow you to add a new well, remove the currently selected well, or load wells from a file.

Each well tab contains space for the following information:

- *Name*: The name to be used for the well in plots
- *Location*: The location of the well in UTM (for now only vertical wells are considered)
- *Pump Start Time*: The absolute timestamp indicating when the well was turned on
- *Flow Rate*: The average flow rate for the well (with positive values indicating injection)
- *Flow File*: An optional file that contains time-varying pumping information, which will be used instead of the average flow rate
- *Monitor Pressure*: An optional flag to indicate whether the pressure should be queried at this point 

The flow file is a CSV format file, which contains time and flow rate information.
The headers indicate which data column corresponds to each value, and the units of each parameter (with exponents indicated via the `**` operator).
For example, see *orion/data/Oklahoma_Cushing2014/example_flow_file.csv* in the `orion` repository:

.. literalinclude:: ./example_inputs/example_flow_file.csv


The bulk well file is a CSV format file, with a single-line header, and the following columns: *well_name*, *x*, *y*, *z*, *init_time*, *flow_rate*, *pressure*, *flow_file*.

.. image:: ./figures/configure_fluid_injection.png


Well Database Configuration
---------------------------

The *Well Database* page contains options to obtain well information from external sources:

- *Data Source*: Select the source of the data (Note: only the Oklahoma Corporation Commission is currently available)
- *Request Range*: Set the start/stop time for the well data request (If either of these are empty, then Orion will choose the maximum/minimum available times for the dataset)
- *Autopick Time Buffer*: For pressure calculations, use well data this amount of time before/after the grid

The local well database is stored in the orion cache directory (*~/.cache/orion/*) in an HDF5 format.
The buttons at the bottom of the page do the following:

- *Update data*: Request well data using the current configuration and update the database
- *Clear data*: Remove any existing well data
- *Autopick wells*: Add any available wells within the grid to the pressure calculation

.. image:: ./figures/configure_well_database.png


Geologic Model Configuration
----------------------------

The *Geologic Model* page contains information about the reservoir, such as permeability and in-situ stress conditions.
These include options for values that are uniform across the reservoir, and those that are heterogeneous (see :ref:`Table Files`).
Note: If a heterogeneous parameter file is specified, these values will override the uniform values.

.. image:: ./figures/configure_geologic_model.png


Pressure Models
===============

The following fluid pressure models are implemented in Orion:

.. toctree::
   :maxdepth: 1

   pressure_models/radial_flow

   pressure_models/table_based

   pressure_models/ml_tang


Seismic Forecast Models
=======================

The following seismic forecast models are implemented in Orion:

.. toctree::
   :maxdepth: 1

   forecast_models/coupled_coulomb_rate_state

   forecast_models/pretrained_ml

   forecast_models/rate_and_state_ode


Ensemble Forecast Calculation
=============================

The performance of individual seismic forecasting method can vary depending on site conditions and/or user configuration.
To address this, Orion generates an 'ensemble' or best-guess forecast using one of the following methods:

- Linear Grid: This approach uses a small period of test data in the user-defined grid to estimate the optimal weight and bias for active forecast models. These resultant weights can then be inspected and tuned in the configuration menu.
- ML Style: This method is under development. Our goal is to take a model that is pre-trained on historical data to produce the ensemble forecast. If desired, this model can then be updated to better match site conditions using transfer learning.




Frequently Asked Questions
==========================

Please send any question to the `Orion development team <https://gitlab.com/NRAP/orion/-/graphs/develop>`_.
