
Contributing to Orion
===========================

Code Style
---------------------------

Orion uses the YAPF tool to create a uniform code style that is compliant with PEP8 standards (with the maximum line length increased from 80 to 120 characters).
YAPF can be installed via pip:

.. code-block:: bash

    pip install yapf


To check your code for compliance or to re-format it with YAPF, you can do the following:

.. code-block:: bash

    # Move into the Orion repository
    cd orion

    # Check for any lines that need to be updated
    yapf -r --diff .

    # Apply the formatter
    yapf -r -i .


If parts of your code are not handled well by the formatter, you can selectively turn it on/off as follows:

.. code-block:: python

    # yapf: disable
    some_large_datastructure = {}
    some_large_list = [[1, 2, 3],
                       [4, 5, 6]]
    # yapf: enable


In addition, when you push changes to a branch, the gitlab CI will automatically check the formatting of the code.
If any lines are out of compliance, they will trigger a `testing/style` pipeline failure.
To determine which lines need to be updated, you can follow the above steps, or you can click on the pipeline tag (something like `#695942271`) at the top of the merge request and then click on the `style` icon.


