Rate-and-State ODE
==================

The rate-and-state earthquake nucleation model estimates the number of independent events in response to a change in stress and can be described by the following ordinary differential equation :cite:`dieterich1994constitutive,segall2015injection`:

.. math::

    \frac{dR}{dt} = \frac{R}{t_c} \left( \frac{\dot{\tau}}{\dot{\tau_0}} - R \right)

where :math:`R` is the ratio between the seismicity rate relative to the background rate, :math:`t_c = \frac{A\sigma}{\dot{\tau_0}}` is the characteristic relaxation time, :math:`\dot{\tau}` and :math:`\dot{\tau_0}` are the Coulomb and background stressing rates, respectively. The ordinary differential equation is solved using a fifth order adaptive time step Runge-Kutta-Fehlberg algorithm :cite:`fehlberg1969low`.


Model parameters
----------------

- Coefficient of friction, :math:`\mu`
- Background seismicity rate, :math:`r_0` (events/year)
- Background stress rate, :math:`\dot{\tau_0}` (MPa/year)
- Free parameter, :math:`A\sigma` (MPa)


Solver parameters
-----------------

- Initial time step size (day): step size for the first iteration. The step size will be either increased or decreased depending on the convergence at the current time step
- Maximum time step size (day): maximum allowed step size, the step size can not be larger than this value
- Time step reduction factor: factor by which the step size is reduced after convergence failure at the current time step
- Maximum number of time step reduction: maximum number of consecutive time step reductions, the solver is stopped if this limit is reached. In this case, the user can either decrease the initial time step size or time step reduction factor, or increase this limit
- Relative convergence tolerance: convergence criterion


.. image:: ../figures/configure_forecast_rsode.png

