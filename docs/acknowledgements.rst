################
Acknowledgements
################

ORION was developed with funding support from the United States Department of Energy’s Office of Fossil Energy and Carbon Management through the Science-informed Machine Learning to Accelerate Real-Time (SMART) Decisions in Subsurface Applications Initiative and the National Risk Assessment Partnership (NRAP). The work was funded, in part, through the Bipartisan Infrastructure Law.

This support is gratefully acknowledged.

This work was performed under the auspices of the U.S. Department of Energy by Lawrence Livermore National Laboratory under contract DE-AC52-07NA27344, and is released under the identifier LLNL-CODE-842148.
