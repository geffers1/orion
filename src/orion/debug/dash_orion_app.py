import sys
import os
from pathlib import Path
from dash import Dash, dcc, html
from dash.dependencies import Input, Output, State
import numpy as np
import pandas as pd
import plotly.express as px
import sys
import os
from pathlib import Path

mod_path = Path(__file__).resolve().parents[2]
sys.path.append(os.path.abspath(mod_path))
from orion.examples import built_in_manager
from orion.managers import orion_manager


def setup_orion(clear_cache=True, built_in='Decatur', config_name='', variant_id='', profile_run=False):
    # To prevent any issues, you may need to remove existing cache files
    cache_root = os.path.expanduser('~/.cache/orion')
    cache_file = os.path.join(cache_root, 'orion_config.json')
    os.makedirs(cache_root, exist_ok=True)
    if clear_cache and os.path.isfile(cache_file):
        print('(clearing cache before run)')
        os.remove(cache_file)

    # Optionally choose one of the built in files
    if built_in:
        config_name = ''
        built_in_manager.compile_built_in(built_in + variant_id, cache_file)

    # Launch orion
    return orion_manager.OrionManager(config_fname=config_name)


def get_orion_df(manager):
    # Build a plot
    grid = manager.children['GridManager']
    catalog = manager.children['SeismicCatalog']
    catalog.reset_slice()
    catalog.set_origin(grid)
    x, y, z = catalog.get_plot_location(grid)
    magnitude = catalog.magnitude_slice
    magnitude_point_size = catalog.get_scaled_point_size(magnitude)
    t_scale = 60 * 60 * 24.0

    # Setup the figure
    df = pd.DataFrame(
        data={
            'magnitude_point_size': magnitude_point_size,
            'magnitude': magnitude,
            'x': catalog.easting_slice,
            'y': catalog.northing_slice,
            'z': catalog.depth_slice,
            'latitude': catalog.latitude_slice,
            'longitude': catalog.longitude_slice,
            'time': catalog.relative_time / t_scale
        })

    return df


# Setup the app
manager = setup_orion(built_in='Decatur')
manager.children['ForecastManager'].use_multiprocessing = False
grid = manager.children['GridManager']
grid.spatial_type = 'Lat Lon'
grid.switch_style()
lon_range, lat_range = grid.get_plot_range()

# manager.run()
df = get_orion_df(manager)

# fig = px.scatter(df, x="x", y="y",
#                  size="magnitude_point_size", color="time",
#                  hover_data=["magnitude"], render_mode='svg',
#                  width=800, height=800)
# hover_data=["magnitude", "time", "depth"],

ds = pd.Series(data={'%1.1f': 'magnitude', '%1.2f': 'time', False: 'magnitude_point_size'})

fig = px.scatter_mapbox(df,
                        lat="latitude",
                        lon="longitude",
                        hover_data=ds,
                        size="magnitude_point_size",
                        color="time",
                        width=700,
                        height=500)

fig.update_layout(
    mapbox_style="white-bg",
    mapbox_layers=[{
        "below":
        'traces',
        "sourcetype":
        "raster",
        "sourceattribution":
        "United States Geological Survey",
        "source": ["https://basemap.nationalmap.gov/arcgis/rest/services/USGSImageryOnly/MapServer/tile/{z}/{y}/{x}"]
    }],
    margin={
        "r": 0,
        "t": 0,
        "l": 0,
        "b": 0
    })

# fig.update_layout(mapbox_bounds={"west": lon_range[0], "east": lon_range[1],
#                                  "south": lat_range[0], "north": lat_range[1]})

# Setup the app
app = Dash(__name__)

app.layout = html.Div([dcc.Graph(id='catalog', figure=fig)])

if __name__ == '__main__':
    app.run_server(debug=True)
