# ------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: MIT
#
# Copyright (c) 2020-, Lawrence Livermore National Security, LLC
# All rights reserved
#
# See top level LICENSE, COPYRIGHT, CONTRIBUTORS, NOTICE, and ACKNOWLEDGEMENTS files for details.
# ------------------------------------------------------------------------------------------------

import sys
import os
from pathlib import Path

mod_path = Path(__file__).resolve().parents[2]
sys.path.append(os.path.abspath(mod_path))
from orion.examples import built_in_manager


def debug_orion(run_gui=True, clear_cache=True, built_in='', config_name='', variant_id='', profile_run=False):
    """
    Trigger a run to debug orion
    Args:
        run_gui (bool): Launch the gui
        clear_cache (bool): Clear the orion cache before running
        built_in (str): Name of a built-in debug case
        config_name (str): Configuration file (overriden by built_in)
        variant_id (str): Identifier for built in variant
    """

    # To prevent any issues, you may need to remove existing cache files
    cache_root = os.path.expanduser('~/.cache/orion')
    cache_file = os.path.join(cache_root, 'orion_config.json')
    os.makedirs(cache_root, exist_ok=True)
    if clear_cache and os.path.isfile(cache_file):
        print('(clearing cache before run)')
        os.remove(cache_file)

    # Optionally choose one of the built in files
    if built_in:
        config_name = ''
        built_in_manager.compile_built_in(built_in + variant_id, cache_file)

    # Launch orion
    if run_gui:
        from orion.gui import orion_gui
        orion_gui.launch_gui(config_name, profile_run=profile_run)
    else:
        from orion.managers import orion_manager
        orion_manager.run_manager(config_name)


def debug_download():
    example_manager = built_in_manager.BuiltInExampleManager()
    example_manager.download_examples = ['Decatur']
    example_manager.download_data()


if (__name__ == '__main__'):
    debug_orion(clear_cache=True)
    # debug_orion(config_name='test.json')
    # debug_orion(built_in='Decatur')
    # debug_orion(built_in='Decatur', profile_run=True)
    # debug_orion(built_in='BayArea')
    # debug_orion(built_in='Oklahoma_Cushing2014')
    # debug_orion(run_gui=False)
    # debug_orion(run_gui=False, built_in='Oklahoma_Cushing2014')
    # debug_orion(built_in='Oklahoma_Cushing2014', variant_id='Wells')
    # debug_orion(built_in='Oklahoma_Cushing2014')

    # debug_orion(config_name='test.json')
    # debug_download()
